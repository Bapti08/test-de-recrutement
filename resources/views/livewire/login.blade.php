<div>
    <div class="flex h-screen bg-white">
        <div class="flex flex-col justify-center flex-1 px-4 py-12 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
            <div class="w-full max-w-sm mx-auto lg:w-96">
                <div class="flex justify-center">
                    <form wire:submit.prevent="login">
                        <div class="flex flex-col">
                            <label class="block font-bold text-sm text-gray-700 dark:text-gray-200" for="identifier">E-mail</label>
                            <input wire:model='email' class="p-2 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-200 border border-gray-300 focus:border-cyan-500 focus:ring focus:ring-cyan-500 focus:ring-opacity-50 rounded-md shadow-sm block mt-1 w-full" type="text" name="identifier" id="identifier">
                            @error('email') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                        </div>
                        <div class="flex flex-col mt-4">
                            <label class="block font-bold text-sm text-gray-700 dark:text-gray-200" for="password">Password</label>
                            <input wire:model='password' class="p-2 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-200 border border-gray-300 focus:border-cyan-500 focus:ring focus:ring-cyan-500 focus:ring-opacity-50 rounded-md shadow-sm block mt-1 w-full" type="password" name="password" id="password">
                            @error('password') <span class="text-red-500 text-sm">{{ $message }}</span> @enderror
                        </div>
                        <div class="mt-4 w-full flex justify-end">
                            <input type="submit" class="px-4 py-1 bg-blue-500 text-white rounded-md hover:cursor-pointer hover:bg-blue-600 transition-colors" value="Submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="relative flex-1 hidden w-0 lg:block">
            <img class="absolute inset-0 z-0 object-cover w-full h-full" src="https://source.unsplash.com/collection/54581509/1920x1080" alt="img">
        </div>
    </div>
</div>
