<div>
    <div class="w-2/3 h-full mx-auto flex justify-center align-center gap-4">
        <div class="w-1/4 flex flex-col">
            <label class="block font-bold text-sm text-gray-700 dark:text-gray-200" for="identifier">Search</label>
            <div class="mt-1 flex gap-2 items-center justify-between">
                <input type="search" list="movies-purpose" wire:model='search' class="p-2 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-200 border border-gray-300 focus:border-cyan-500 focus:ring focus:ring-cyan-500 focus:ring-opacity-50 rounded-md shadow-sm block w-full">
                <datalist id="movies-purpose">
                    @if ($searchPurpose != null)
                        @foreach ($searchPurpose as $purpose)
                            <option value="{{ $purpose->title }}">
                        @endforeach
                    @endif
                </datalist>
                <button class="rounded-full w-12 h-10 bg-blue-500 flex justify-center items-center" wire:click='validateSearch'>
                    <svg class="w-5" viewBox="0 0 384 512"><path fill="#FFFFFF" d="M342.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L274.7 256 105.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z"/></svg>
                </button>
            </div>
        </div>
        <div class="px-6 py-4 h-full h-[calc(100vh-100px)] w-3/4 flex flex-col border-4 border-dashed border-gray-300 rounded-lg overflow-y-scroll">
            @if($searchResult->count() == 0)
                <h4 class="mb-4 block font-bold text-xl text-gray-700 dark:text-gray-200">Most popular movies</h4>
                <div class="flex flex-wrap gap-4 justify-center">
                    @foreach ($popularMovies as $movie)
                        <x-movie-card wire:key="popular-{{ $movie->id }}" :id="$movie->id" :img="$movie->img_path" :title="$movie->title" :mean="$movie->notes()->count() != 0 ? $movie->notes_sum_note / $movie->notes()->count() : 0" />
                    @endforeach
                </div>
                <div class="mt-8">
                    {{ $popularMovies->links() }}
                </div>
            @else
                <h4 class="mb-4 block font-bold text-xl text-gray-700 dark:text-gray-200">Search results</h4>
                <div class="flex flex-wrap gap-4 justify-center">
                    @foreach ($searchResult as $result)
                        <x-movie-card wire:key="search-{{ $result->id }}" :id="$result->id" :img="$result->img_path" :title="$result->title" :mean="$result->notes()->count() != 0 ? $result->notes_sum_note / $result->notes()->count() : 0" />
                    @endforeach
                </div>
            @endif
        </div>

        @if ($selection != null)
            <x-movie-modal :movie="$selection" :mean="$selection->notes()->count() != 0 ? $selection->notes_sum_note / $selection->notes()->count() : 0" />
        @endif
    </div>
</div>
