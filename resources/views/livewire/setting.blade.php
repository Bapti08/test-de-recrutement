<div class="w-2/3 h-full mx-auto flex justify-center align-center gap-4">
    <div class="px-6 py-4 h-full h-[calc(100vh-100px)] w-full flex flex-col border-4 border-dashed border-gray-300 rounded-lg overflow-y-scroll">
        <form wire:submit.prevent='save'>
            <div class="flex gap-4 items-start">
                <input type="file" class="sr-only" wire:model='cover' id="cover">
                <label for="cover">
                    @if($cover)
                        <img class="cursor-pointer w-[300px] h-[300px] min-h-1/6 object-cover rounded-md" src="{{ $cover->temporaryUrl() }}" alt="cover">
                    @elseif($updating)
                        <img class="cursor-pointer w-[300px] h-[300px] min-h-1/6 object-cover rounded-md" src="{{ Storage::url($oldCover) }}" alt="cover">
                    @else
                        <div class="cursor-pointer w-[200px] h-[300px] rounded-md border-2 border-dashed flex jusitfy-center items-center">
                            <svg class="w-full h-1/3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path d="M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z"/>
                            </svg>
                        </div>
                    @endif
                    @error('cover') <span class="text-sm text-red-500 font-semibold">{{ $message }}</span> @enderror
                </label>
                <div class="w-full flex flex-col">
                    <div class="w-full flex flex-col">
                        <label class="mb-2 block font-bold text-sm text-gray-700 dark:text-gray-200" for="title">Title</label>
                        <input type="title" wire:model.defer='film.title' class="w-full p-2 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-200 border border-gray-300 focus:border-cyan-500 focus:ring focus:ring-cyan-500 focus:ring-opacity-50 rounded-md shadow-sm block w-full">
                        @error('film.title') <span class="text-sm text-red-500 font-semibold">{{ $message }}</span> @enderror
                    </div>
                    <div class="mt-2 w-full flex flex-col">
                        <label class="mb-2 block font-bold text-sm text-gray-700 dark:text-gray-200" for="">Available genres</label>
                        <div class="flex items-center gap-2">
                            @foreach ($availableGenres as $key => $genre)
                                <div wire:click='addGenre({{ $key }})' class="cursor-pointer py-1 px-3 rounded-md" style="background-color: {{ $genre['color'] }}">{{ $genre['name'] }}</div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mt-2 mb- w-full min-h-[100px] flex flex-col">
                        <label class="mb-2 block font-bold text-sm text-gray-700 dark:text-gray-200" for="">Selected genres</label>
                        <div class="flex items-center gap-2">
                            @foreach ($selectedGenres as $key => $genre)
                                <div wire:click='removeGenre({{ $key }})' class="cursor-pointer py-1 px-3 rounded-md" style="background-color: {{ $genre['color'] }}">{{ $genre['name'] }}</div>
                            @endforeach
                        </div>
                        @error('genres') <span class="text-sm text-red-500 font-semibold">{{ $message }}</span> @enderror
                    </div>
                    <button type="submit" class="w-min px-5 py-2 bg-blue-500 text-white rounded-md">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
