<div wire:click='setSelection({{ $id }})' class="w-1/4 flex flex-col bg-gray-400 rounded-md">
    <img class="w-[300px] h-[300px] min-h-1/6 object-cover rounded-md" src="{{ Storage::url($img) }}" alt="cover">
    <div class="mt-2 w-full flex justify-between items-center py-2 px-3 gap-2">
        <h5 class="text-md font-semibold truncate">{{ $title }}</h5>
        <div class="h-5 flex justify-center gap-1">
            <p class="font-semibold text-sm">{{ number_format($mean, 2, ',', ' ') }}</p>
            <svg class="h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 260 245">
                <path fill="#FFFF00" d="m56,237 74-228 74,228L10,96h240"/>
            </svg>
        </div>
    </div>
</div>
