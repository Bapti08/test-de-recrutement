<div id="defaultModal" tabindex="-1" class="mx-auto fixed top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 z-50">
    <div class="relative p-4 min-w-[800px] max-w-[800px] h-full md:h-auto">
        <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
            <div class="flex justify-between items-start p-4 rounded-t border-b dark:border-gray-600">
                <div class="w-full flex justify-between items-center">
                    <div class="flex items-center justify-between">
                        <div class="flex items-center gap-4">
                            <div class="flex flex-col gap-1 justify-center">
                                <h3 class="text-xl font-semibold text-gray-900 dark:text-white">{{ $movie->title }}</h3>
                                <div class="h-5 flex items-center gap-1">
                                    <p class="font-semibold text-sm">{{ $movie->notes()->count() != 0 ? $movie->notes_sum_note / $movie->notes()->count() : 0 }}</p>
                                    <svg class="h-4" viewBox="0 0 260 245">
                                        <path fill="#DFDF00" d="m56,237 74-228 74,228L10,96h240"/>
                                    </svg>
                                </div>
                            </div>
                            <div class="flex items-center gap-1">
                                @foreach ($movie->genres as $genre)
                                    <span class="px-2 py-1 rounded-md text-white" style="background-color: {{ $genre->color }}">{{ $genre->name }}</span>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center gap-1">
                        @php
                            $object = $movie->notes()->where('user_id', auth()->id())->first();
                            $currentNote = $object != null ? $object->note : 0;
                        @endphp
                        @for ($i = 1; $i <= 5; $i++)
                            <button wire:click='note({{ $i }})'>
                                <svg class="h-5" viewBox="0 0 260 245">
                                    <path fill="@if($currentNote >= $i) #DFDF00 @else #000000 @endif" d="m56,237 74-228 74,228L10,96h240"/>
                                </svg>
                            </button>
                        @endfor
                    </div>
                </div>
                <div class="flex items-center">
                    @if(auth()->user()->is_admin)
                        <button wire:click='delete({{ $movie->id }})' class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg class="h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M135.2 17.7L128 32H32C14.3 32 0 46.3 0 64S14.3 96 32 96H416c17.7 0 32-14.3 32-32s-14.3-32-32-32H320l-7.2-14.3C307.4 6.8 296.3 0 284.2 0H163.8c-12.1 0-23.2 6.8-28.6 17.7zM416 128H32L53.2 467c1.6 25.3 22.6 45 47.9 45H346.9c25.3 0 46.3-19.7 47.9-45L416 128z"/></svg>
                            <span class="sr-only">Update current</span>
                        </button>
                        <a href="{{ route('setting', ['id' => $movie->id]) }}" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg class="h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M495.9 166.6c3.2 8.7 .5 18.4-6.4 24.6l-43.3 39.4c1.1 8.3 1.7 16.8 1.7 25.4s-.6 17.1-1.7 25.4l43.3 39.4c6.9 6.2 9.6 15.9 6.4 24.6c-4.4 11.9-9.7 23.3-15.8 34.3l-4.7 8.1c-6.6 11-14 21.4-22.1 31.2c-5.9 7.2-15.7 9.6-24.5 6.8l-55.7-17.7c-13.4 10.3-28.2 18.9-44 25.4l-12.5 57.1c-2 9.1-9 16.3-18.2 17.8c-13.8 2.3-28 3.5-42.5 3.5s-28.7-1.2-42.5-3.5c-9.2-1.5-16.2-8.7-18.2-17.8l-12.5-57.1c-15.8-6.5-30.6-15.1-44-25.4L83.1 425.9c-8.8 2.8-18.6 .3-24.5-6.8c-8.1-9.8-15.5-20.2-22.1-31.2l-4.7-8.1c-6.1-11-11.4-22.4-15.8-34.3c-3.2-8.7-.5-18.4 6.4-24.6l43.3-39.4C64.6 273.1 64 264.6 64 256s.6-17.1 1.7-25.4L22.4 191.2c-6.9-6.2-9.6-15.9-6.4-24.6c4.4-11.9 9.7-23.3 15.8-34.3l4.7-8.1c6.6-11 14-21.4 22.1-31.2c5.9-7.2 15.7-9.6 24.5-6.8l55.7 17.7c13.4-10.3 28.2-18.9 44-25.4l12.5-57.1c2-9.1 9-16.3 18.2-17.8C227.3 1.2 241.5 0 256 0s28.7 1.2 42.5 3.5c9.2 1.5 16.2 8.7 18.2 17.8l12.5 57.1c15.8 6.5 30.6 15.1 44 25.4l55.7-17.7c8.8-2.8 18.6-.3 24.5 6.8c8.1 9.8 15.5 20.2 22.1 31.2l4.7 8.1c6.1 11 11.4 22.4 15.8 34.3zM256 336c44.2 0 80-35.8 80-80s-35.8-80-80-80s-80 35.8-80 80s35.8 80 80 80z"/></svg>
                            <span class="sr-only">Update current</span>
                        </a>
                    @endif
                    <button wire:click='resetSelection' class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
            </div>
            <div class="p-6 space-y-6 h-1/2 min-h-[400px] max-h-[400px] overflow-y-scroll">
                @foreach ($movie->comments as $comment)
                    <x-comment-card :comment="$comment" />
                @endforeach
            </div>
            <div class="flex items-center p-6 space-x-2 rounded-b border-t border-gray-200 dark:border-gray-600">
                <form class="flex flex-col items-end gap-2 w-full" wire:submit.prevent='comment'>
                    <textarea type="text" wire:model.defer='newComment' class="w-full p-2 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-200 border border-gray-300 focus:border-cyan-500 focus:ring focus:ring-cyan-500 focus:ring-opacity-50 rounded-md shadow-sm block  w-full"></textarea>
                    <button type="submit" class="px-6 py-1 text-white bg-blue-500 rounded-md">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
