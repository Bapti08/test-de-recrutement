<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="{{ asset('js/app.js') }}"></script>

        @livewireStyles
    </head>

    <body>
        @if (auth()->user() != null)
            <nav class="mx-4 h-14 flex justify-end gap-4">
                @if (auth()->user()->is_admin)
                    <a href="{{ route('setting') }}" class="my-auto px-6 py-2 bg-blue-500 rounded-md shadow-md text-white">Add movie</a>
                @endif
                <a href="{{ route('logout') }}" class="my-auto px-6 py-2 bg-red-500 rounded-md shadow-md text-white">Logout</a>
            </nav>
        @endif
        <div class="h-[calc(100vh-56px)]">
            {{ $slot }}
        </div>

        @livewireScripts
    </body>
</html>
