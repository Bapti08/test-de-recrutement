<div class="w-full flex flex-col rounded-md bg-gray-300 p-2">
    <div class="w-full flex justify-between items-center">
        <h6 class="font-bold text-md">{{ $comment->user->name }}</h6>
        <h6 class="font-bold text-md">{{ $comment->created_at->format('d-m-Y H:i') }}</h6>
    </div>
    <p class="text-sm">{{ $comment->content }}</p>
</div>
