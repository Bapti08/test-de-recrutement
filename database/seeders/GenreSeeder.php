<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = [
            'Action',
            'Aventure',
            'Drame',
            'Science-fiction',
            'Épouvante',
            'Thriller'
        ];

        foreach ($genres as $genre)
            Genre::factory(1)->create([
                'name' => $genre,
            ]);
    }
}
