<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Film;
use App\Models\Genre;
use App\Models\Note;
use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $films = [
            'Avatar',
            'Star wars I',
            'Star wars II',
            'Star wars III',
            'Star wars IV',
            'Star wars V',
            'Star wars VI',
            'Star wars Rogue one',
            'Star wars solo'
        ];

        foreach ($films as $film) {
            $film = Film::create([
                'title' => $film,
                'img_path' => 'films/' . str_replace(' ', '_', strtolower($film)) . '.jpg'
            ]);

            $film->genres()->attach(1);
            $film->genres()->attach(3);

            Note::factory(1)->create([
                'notable_type' => 'film',
                'notable_id' => $film->id,
                'user_id' => 1
            ]);

            Comment::factory(5)->create([
                'commentable_type' => 'film',
                'commentable_id' => $film->id,
                'user_id' => 1
            ]);
        }
    }
}
