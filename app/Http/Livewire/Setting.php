<?php

namespace App\Http\Livewire;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Setting extends Component
{
    use WithFileUploads, AuthorizesRequests;

    public Film $film;

    public $cover;

    public string $oldCover;

    public array $genres = [];

    public array $selectedGenres = [];

    public array $availableGenres = [];

    public bool $updating = false;

    protected $rules = [
        'film.title' => ['required', 'string'],
        'genres' => ['required', 'array', 'min:1'],
        'genres.*' => ['integer', 'exists:genres,id']
    ];

    public function mount(?int $id = null)
    {
        $this->availableGenres = Genre::all()->toArray();

        if ($id != null) {
            $this->film = Film::find($id);
            $this->oldCover = $this->film->img_path;

            $this->selectedGenres = $this->film->genres->toArray();
            $this->availableGenres = Genre::whereNotIn('id', $this->film->genres->pluck('id'))->get()->toArray();
            foreach ($this->selectedGenres as $value)
                $this->genres[] = $value['id'];

            $this->updating = true;
        }
        else
            $this->film = new Film();
    }

    public function save()
    {
        $this->authorize('create', $this->film);

        $this->validateOnly('film.title');

        if ($this->updating)
            $this->validate(['cover' => ['sometimes', 'nullable', 'image']]);
        else
            $this->validate(['cover' => ['required', 'image']]);

        $this->validateOnly('genres');
        $this->validateOnly('genres.*');

        if ($this->cover) {
            if (Storage::exists($this->film->img_path))
                Storage::delete($this->film->img_path);

            $this->film->img_path = $this->cover->store('public/films');
        }

        $this->film->save();

        foreach ($this->genres as $genre)
            $this->film->genres()->attach($genre);

        return redirect('dashboard');
    }

    public function addGenre(int $index)
    {
        $this->selectedGenres[] = $this->availableGenres[$index];
        $this->genres[] = $this->availableGenres[$index]['id'];
        unset($this->availableGenres[$index]);
    }

    public function removeGenre(int $index)
    {
        $buff = $this->selectedGenres[$index];
        unset($this->selectedGenres[$index]);
        $this->availableGenres[] = $buff;

        foreach ($this->genres as $key => $id)
            if ($buff['id'] == $id)
                unset($this->genres[$key]);
    }

    public function render()
    {
        return view('livewire.setting');
    }
}
