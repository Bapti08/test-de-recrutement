<?php

namespace App\Http\Livewire;

use App\Models\Film;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class Dashboard extends Component
{
    use WithPagination, AuthorizesRequests;

    public string $search = '';

    public Collection $searchPurpose;

    public Collection $searchResult;

    public ?Film $selection;

    public string $newComment = '';

    protected $listeners = ['refreshComponent' => '$refresh'];

    protected $rules = [
        'newComment' => ['required', 'string']
    ];

    public function mount()
    {
        $this->searchPurpose = collect();
        $this->searchResult = collect();
    }

    public function updatedSearch()
    {
        if (empty($this->search))
            $this->searchPurpose = collect();
        else
            $this->searchPurpose = Film::where('title', 'like', '%' . $this->search . '%')
                ->limit(5)
                ->get();

        $this->emit('refreshComponent');
    }

    public function validateSearch()
    {
        if (empty($this->search))
            $this->searchResult = collect();
        else
            $this->searchResult = Film::withSum('notes', 'note')
                ->orderBy('notes_sum_note', 'desc')
                ->where('title', 'like', '%' . $this->search . '%')
                ->limit(6)
                ->get();
    }

    public function render()
    {
        return view('livewire.dashboard', [
            'popularMovies' => Film::withSum('notes', 'note')
                ->orderBy('notes_sum_note', 'desc')
                ->paginate(6)
        ]);
    }

    public function setSelection(int $id)
    {
        $this->selection = Film::withSum('notes', 'note')
            ->find($id);
    }

    public function resetSelection()
    {
        $this->selection = null;
    }

    public function comment()
    {
        $this->validateOnly('newComment');

        $this->selection->comments()->create([
            'user_id' => auth()->id(),
            'content' => $this->newComment
        ]);

        $this->newComment = '';

        $this->emit('refreshComponent');
    }

    public function note(int $note)
    {
        if ($this->selection->notes()->where('user_id', auth()->id())->exists()) {
            $previous = $this->selection->notes()->where('user_id', auth()->id())->first();
            $previous->delete();
        }

        $this->selection->notes()->create([
            'user_id' => auth()->id(),
            'note' => $note
        ]);
    }

    public function delete(int $id)
    {
        if ($this->selection != null) {
            $this->authorize('delete', $this->selection);
            $buff = $this->selection;
            $this->selection = null;

            if (Storage::exists($buff->img_path))
                Storage::delete($buff->img_path);

            $buff->delete();
        }
    }
}
