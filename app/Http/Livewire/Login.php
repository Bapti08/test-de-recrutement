<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public string $email = '';

    public string $password = '';

    protected $rules = [
        'email' => ['required', 'email', 'exists:users,email'],
        'password' => ['required', 'string']
    ];

    public function render()
    {
        return view('livewire.login');
    }

    public function login()
    {
        $this->validateOnly('email');
        $this->validateOnly('password');

        if (Auth::attempt(['email' => $this->email, 'password' => $this->password]))
            return redirect('/dashboard');
        else
            $this->addError('password', 'Invalid password');
    }
}
