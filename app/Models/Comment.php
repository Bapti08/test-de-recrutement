<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'content'
    ];

    public static function boot()
    {
        parent::boot();

        Relation::enforceMorphMap([
            'film' => Film::class
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commentable()
    {
        return $this->morphTo('commentable');
    }
}
