<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'img_path'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class, 'films_genres')
            ->withTimestamps();
    }

    public function notes()
    {
        return $this->morphMany(Note::class, 'notable');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
